# Basics

This JavaScript code can be used to create a Twitter bot that tweets a link whenever an article is posted from a given list of RSS feeds. You will need to set up a Twitter account in order to use it.

# Dependencies

* nodejs
* addressparser
* array-indexofobject
* feedparser
* readable-stream
* request
* sax
* simple-twitter

# Installation

* Create a Twitter account and register an app.
* Enter your Twitter app token codes where indicated in rss-twitter-bot.js
* Install [node](https://nodejs.org/)
* Install all other dependencies: `npm install [package-name]`
* From the command line enter `node rss-twitter-bot.js`

# More information

You can find a two-part tutorial on how to set this up at the following links:
* [Part 1](https://www.hughrundle.net/2015/07/16/building-a-twitter-bot-using-node-feedparser-and-simple-twitter-part-1/)
* [Part 2](https://www.hughrundle.net/2015/07/18/building-a-twitter-bot-part-2-its-aliiiive/)

Note that the bot mentioned in these posts now runs on [meteor2twitter](https://gitlab.com/hughr/meteor2twitter)

# License

The MIT License (MIT)

Copyright (c) 2015 Hugh Rundle

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.