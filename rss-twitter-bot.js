
// Require simple-twitter
var twitter = require('simple-twitter');
// Set up the twitter app credentials
 twitter = new twitter( 'xxx', //consumer key from twitter api 
                        'xxx', //consumer secret key from twitter api 
                        'xxx', //acces token from twitter api 
                        'xxx', //acces token secret from twitter api 
                        3600  //(optional) time in seconds in which file should be cached (only for get requests), put false for no caching 
                      );

// Create a simple server
// Really all this 'server' does is ensure our code keeps running
var http = require('http');
http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.end('Aus Blog Feeds Bot\n');
}).listen(8080);

// Set timeout to loop the whole thing every 10 minutes
var timerVar = setInterval (function () {runBot()}, 600000);

// Run the bot!
function runBot() {
  // Get feeds using feedparser
   var FeedParser = require('feedparser')
    , request = require('request'), fs = require('fs');

// get the list of feeds from the feeds file
var feeds = fs.readFileSync('feeds.txt').toString().split('\n');

  // iterate through the array
  feeds.forEach(function(theUrl){
    var req = request(theUrl)
      , feedparser = new FeedParser();

    // deal with any errors in FeedParser
    req.on('error', function (error) {
      console.error(error); 
    });

    req.on('response', function (res) {
      var stream = this;
      // deal with any errors in the feed
      if (res.statusCode != 200) return this.emit('error', new Error('Bad status code'));
      stream.pipe(feedparser);
    });

    // deal with any errors in the code
    feedparser.on('error', function(error) {
      console.error(error); 
    });

    feedparser.on('readable', function() {
      // This is where the action is! 
      var stream = this
          , meta = this.meta
          , item;
      while (item = stream.read()) {
        
        // Get the date and time right now
        var dateNow = new Date();
        // Get the date 11 minutes ago (roughly the last time the bot finished running)
        // Twitter should reject any duplicate posts that slip through if the code runs faster
        var lastRun = dateNow - 660000;
        // Get the date 12 hours ago
        var dateYesterday = dateNow - 43200000;
        // Get the date 12 hours and ten minutes ago
        var dateYPT = dateNow - 43800000;
        // Ensure we only try to post things published between 12 hours and 12 hours 10 minutes ago
        if (item.date > dateYPT && item.date < dateYesterday){
          // Here we are ensuring that long post titles don't lose the link in the tweet.
          var titleLength = item.title.length;
          var itemTitle = item.title;
          // if the title is shorter than 70 characters, post it to twitter as is.
          if (titleLength < 70) {
            twitter.post('statuses/update',
                            {'status' : item.title + ' | ' + item.author + ' | ' + item.link},
                            // deal with any twitter errors
                            function(error, data) {
                              console.dir(data);
                            }     
                        );
           }
          // If the title is longer than 70 characters we truncate it.
          else {
            trimmedTitle = itemTitle.substring(0, 70);
            twitter.post('statuses/update',
                    {'status' : trimmedTitle + ' | ' + item.author + ' | ' + item.link},
                      // deal with any twitter errors
                      function(error, data) {
                          console.dir(data);
                      }
            );
          };
        }
        // if the publication date was 11 minutes ago or less (i.e. since we last ran the bot)
        if (item.date > lastRun){
          // Here we are ensuring that long post titles don't lose the link in the tweet.
          var titleLength = item.title.length;
          var itemTitle = item.title;
          // if the title is shorter than 70 characters, post it to twitter as is.
          // we also change the separators so Twitter doesn't reject it as a duplicate post
          if (titleLength < 70) {
            twitter.post('statuses/update',
                            {'status' : item.title + ' - ' + item.author + ' - ' + item.link},
                            // deal with any twitter errors
                            function(error, data) {
                              console.dir(data);
                            }     
                        );
           }
          // If the title is longer than 70 characters we truncate it.
          // we also change the separators so Twitter doesn't reject it as a duplicate post
          else {
            trimmedTitle = itemTitle.substring(0, 70);
            twitter.post('statuses/update',
                    {'status' : trimmedTitle + ' - ' + item.author + ' - ' + item.link},
                      // deal with any twitter errors
                      function(error, data) {
                          console.dir(data);
                      }
            );
          };
        }
      };
    });
  });
  // log when the loop is completed so you know the process has run.
  var dateCompleted = new Date();
  console.log('loop completed at ' + dateCompleted);
};


